import React, {Component} from "react";
import '../../containers/Styles/AboutPage.css'
import SubscribeForNewsletter from "./SubscribeForNewsletter";
import AboutPageCard from "./AboutPageCard";


class AboutPage extends Component {


    render() {

        return (
            <div className="container">
                <div className="row">
                    <div className="padded-row"/>
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        <AboutPageCard/>
                    </div>
                    <div className="col-sm-6">
                        <SubscribeForNewsletter/>
                    </div>
                </div>
                <div className="row">
                    <div className="padded-row"/>
                </div>
                <div className="row">
                    <div className="padded-row"/>
                </div>
            </div>
        )
    }
}

export default AboutPage;