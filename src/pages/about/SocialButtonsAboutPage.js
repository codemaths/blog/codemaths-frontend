import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
    button: {
        margin: theme.spacing(1),
    },
    input: {
        display: 'none',
    },
}));

export default function SocialButtons() {
    const classes = useStyles();

    return (
        <div className="social-buttons">
            <Button href="https://gitlab.com/vorashilf"  variant='contained' color="primary" className={classes.button}>
                Gitlab
            </Button>
            <Button href="https://twitter.com/vorashil" variant='contained' color="primary" className={classes.button}>
                Twitter
            </Button>
            <Button href="https://www.linkedin.com/in/vfarzaliyev/" variant='contained' color="secondary" className={classes.button}>
                Linkedin
            </Button>
            <Button href="mailto:vofarzaliyev@gmail.com"  variant='contained' className={classes.button}>
                E-Mail
            </Button>
            <Button href={`${process.env.REACT_APP_CDN}/pdf/mycv.pdf`}  variant='contained' className={classes.button}>
                My CV
            </Button>

        </div>
    );
}