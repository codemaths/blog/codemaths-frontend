import React, {Component} from "react";
import "../../containers/Styles/PostCard.css"
import vorashilAboutImage from "../../resources/aboutPageImage.JPG"
import SocialButtons from "./SocialButtonsAboutPage";

class AboutPageCard extends Component {
    render() {
        return (
            <div>
                <div className="about-card">
                    <div className="container">
                        <div className="row">
                            <div className="col-4">
                                <img
                                    id="about-card-image"
                                    src={vorashilAboutImage}
                                    alt="I am in an open air coffee shop."/>
                            </div>
                            <div className="col-8">
                                <p>
                                    Hi there, my name is Vorashil.
                                    <br/>
                                    I am a software engineer with a degree in mathematics. I love to learn and share my knowledge on these 2 fields, so I created CodeMaths.
                                    All code for this website is publicly available on my Gitlab. Enjoy!
                                </p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="padded-row"/>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <SocialButtons/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AboutPageCard;