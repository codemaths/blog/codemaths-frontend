import React from "react";

const FormPage = () => {
    return (
        <div className="about-card">
            <div className="container">
            <form className="simple-subscription-form">
                <h1>Subscribe to CodeMaths</h1>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Your Name</label>
                    <input disabled="true" type="text" className="form-control" placeholder="Your Name"/>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Email address</label>
                    <input disabled="true" type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                           placeholder="Enter email"/>

                </div>
                <button disabled="true" type="submit" color="indigo" className="btn btn-primary">Submit</button>
            </form>
            </div>
        </div>
    );
};

export default FormPage;