import React, {Component} from "react";
import '../../containers/Styles/PostPageImage.css'

class PostPageImage extends Component {
    render() {
        return (
            <div className="post-page-image">
                <img src={`${process.env.REACT_APP_CDN}/images/${this.props.imageId}.jpg`}
                     id="post-image"
                     className="img-fluid"
                     alt='programming syntax on screen'/>
            </div>
        )
    }
}

export default PostPageImage