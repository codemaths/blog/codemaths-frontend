import React, {Component} from "react";
import CommentCard from "./CommentCard";
import CommentAdd from "./CommentAdd";

class CommentSection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comments: [],
            fetchedComments: false,
        }
    }

    componentDidMount() {
        fetch(process.env.REACT_APP_BACKEND + '/post/' + this.props.postId + '/comments')
            .then(response => {
                if (response.status === 200) {
                    return response.json();
                } else {
                    return [];
                }
            })
            .then(comments => {
                this.setState({comments: comments})
            });
        this.setState({fetchedComments: true})
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <h1>Comments:</h1>
                    <div className="col-sm-12">
                        <div>
                            {this.state.comments.map(comment => {
                                return (
                                    <CommentCard key={comment.id} id={comment.id} author={comment.author} date_added={comment.date_added} content={comment.content}/>
                                )})}
                        </div>

                    </div>
                </div>
                <div className="row">
                    <h1>Add you comment:</h1>
                    <div className="col-sm-12">
                        <CommentAdd postId={this.props.postId}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default CommentSection;