import React, {Component} from "react";
import Divider from "@material-ui/core/Divider";
import "../../containers/Styles/CommentCard.css"
import avatar from "../../resources/avatarForComments.png";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ReactMarkdown from "react-markdown";
import CodeBlock from "../../components/CodeBlock";
import moment from 'moment';

class CommentCard extends Component {
    render() {
        const author = this.props.author;
        const date_added = this.props.date_added;
        const content = this.props.content;
        return (
            <div className="comment-card">
                <ExpansionPanel defaultExpanded={true}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon/>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <div className="containter">
                            <div className="row">
                                <div className="col-3">
                                    <img
                                        id="comment-card-image"
                                        src={avatar}
                                        alt="Avatar for comment box"/>
                                </div>
                                <div className="col-9">
                                    <div className="row">
                                        <div className="col-sm-6">
                                            {author}
                                        </div>
                                        <div className="col-sm-6">
                                            {moment(date_added).format("MMM DD, YYYY")}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ExpansionPanelSummary>
                    <Divider/>
                    <ExpansionPanelDetails>
                        <Typography>
                            <ReactMarkdown source={content} escapeHtml={true} renderers={{ code: CodeBlock }}/>

                        </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        )
    }
}

export default CommentCard