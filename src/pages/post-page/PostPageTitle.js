import React, {Component} from "react";
import '../../containers/Styles/PostPageTitle.css'
import Typography from "@material-ui/core/Typography";

class PostPageTitle extends Component {

    render() {
        return (
            <div>
                <div className="post-page-title">
                    <Typography variant="h3" gutterBottom>
                    {this.props.title.title}
                    </Typography>
                    <Typography variant="subtitle1" gutterBottom>
                        {this.props.title.subtext}
                    </Typography>
                </div>
            </div>
        )
    }
}

export default PostPageTitle;