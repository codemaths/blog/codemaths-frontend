import React, {Component} from "react";
import PostPageTitle from "./PostPageTitle";
import PostPageImage from "./PostPageImage"
import '../../containers/Styles/PostPage.css'
import PostPageContent from "./PostPageContent";
import SocialButtonsShare from "../../components/SocialButtonsShare";
import CommentSection from "./CommentSection";
import SimplePostCard from "./SimplePostCard";

class PostPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            otherPosts: [],
            currentPost: {},
        };
        this.postUrl = process.env.REACT_APP_BACKEND + "/post/" + props.match.params.id;
    }

    componentDidMount() {
        fetch(this.postUrl)
            .then(response => {
                return response.json();
            })
            .then(posts => {
                this.setState({currentPost: posts});
            });
        this.setState({fetchedPost: true})

        fetch(process.env.REACT_APP_BACKEND + '/posts')
            .then(response => {
                return response.json();
            })
            .then(otherPost => {
                this.setState({otherPosts: otherPost})
            });

    }



    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-8">
                        <div className="post-page">
                            <PostPageTitle title={this.state.currentPost}/>
                            <PostPageImage imageId={this.state.currentPost.id}/>
                            <SocialButtonsShare postId={this.props.match.params.id}/>
                            <PostPageContent content={this.state.currentPost.maincontent}/>
                            <CommentSection postId={this.props.match.params.id}/>
                            {/*<p>This is post number {params.id}</p>*/}
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <h1>Check out other articles</h1>
                        {this.state.otherPosts.map(post => {
                            if (post.id !== this.state.currentPost.id) {
                                return (
                                    <SimplePostCard key={post.id} id={post.id} picture={post.title} title={post.title}
                                                    content={post.subtext}/>)

                            }})}
                    </div>
                </div>
            </div>
        )
    }
}

export default PostPage;