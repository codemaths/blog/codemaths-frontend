import React, {Component} from "react";
import '../../containers/Styles/ComentAdd.css'
import {CustomMarkedArea} from "../../components/markdown-text-area/CustomMarkedArea";

class CommentAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            author: '',
            email: '',
            content: ''
        };
        this.commentUrl = process.env.REACT_APP_BACKEND + "/post/" + props.postId + "/comments";
        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
    }

    changeHandler(event) {
        this.setState({[event.target.name]: event.target.value})
    }

    changeHandlerForComment = (value) => {
        this.setState({content: value})
    };

    submitHandler(event) {
        this.postComment().then(() => {
            this.props.history.push('/post/'+this.props.postId)
        }).catch((error) => {
            console.log(error)
        })
    }

    async postComment() {
        try {
            await fetch(this.commentUrl, {
                method: "POST",
                redirect: 'follow',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    author: this.state.author,
                    email: this.state.email,
                    content: this.state.content,
                })
            });
        } catch (e) {
            console.log(e)
        }
    }

    render() {
        const {author, email, content} = this.state;
        return (
            <div className="comment-add">
                <form onSubmit={this.submitHandler}>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="formGroupExampleInput">Your name</label>
                                    <input required
                                           type="text"
                                           className="form-control"
                                           id="formGroupExampleInput"
                                           placeholder="Example input"
                                           name="author"
                                           value={author}
                                           onChange={this.changeHandler}
                                    />
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Your email</label>
                                    <input required
                                           type="email"
                                           className="form-control"
                                           id="exampleFormControlInput1"
                                           placeholder="name@example.com"
                                           name="email"
                                           value={email}
                                           onChange={this.changeHandler}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-10">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlTextarea1">Comment (Markdown enabled)</label>
                                    <CustomMarkedArea
                                        showPreview={true}
                                        characterLimit={500}
                                        name="comment"
                                        value={content}
                                        onChange={this.changeHandlerForComment}
                                    />
                                </div>
                            </div>
                            <div className="col-sm-2 align-self-center">
                                <button type="submit" className="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        )
    }

}

export default CommentAdd