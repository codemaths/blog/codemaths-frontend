import React, {Component} from "react";
import '../../containers/Styles/PostPageContent.css'
import ReactMarkdown from "react-markdown";
import CodeBlock from "../../components/CodeBlock";

class PostPageContent extends Component {
    render() {
        const input = this.props.content;
        return (
            <div className="post-page-content">
                <ReactMarkdown source={input} escapeHtml={false} renderers={{ code: CodeBlock }}/>
            </div>
        )
    }
}

export default PostPageContent;