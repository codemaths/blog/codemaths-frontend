import React, {Component} from "react";
// import NewPostCard from "./NewPostCard";
import PostCard from "./PostCard";


class PostCardList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            fetchedPosts: false,
        }
    }

    componentDidMount() {
        fetch(process.env.REACT_APP_BACKEND + '/posts')
            .then(response => {
                return response.json();
            })
            .then(posts => {
                this.setState({posts: posts})
            });
        this.setState({fetchedPosts: true})
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        {this.state.posts.map(post => {
                            return (
                                <PostCard key={post.id} id={post.id} picture={post.title} title={post.title}
                                          content={post.subtext}/>
                      )})}
                    </div>
                </div>
            </div>

        )
    }
}

export default PostCardList