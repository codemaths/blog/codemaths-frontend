import React, {Component} from "react";
import "../../containers/Styles/PostCard.css"
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import SocialButtonsShare from "../../components/SocialButtonsShare";

class PostCard extends Component {
    render() {
        return (
            <div>
                <div className="post-card">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-6">
                                <a href={`/posts/${this.props.id}`}>
                                    <img
                                        id="post-card-image"
                                        src={`${process.env.REACT_APP_CDN}/images/${this.props.id}.jpg`}
                                        alt="Dimly lit room with a computer interface terminal."/>
                                </a>
                            </div>
                            <div className="col-sm-6">
                                <div className="post-card-title">
                                    <a href={`/posts/${this.props.id}`}>
                                        <Typography variant="h4" gutterBottom>
                                            {this.props.title}
                                        </Typography>
                                    </a>
                                </div>
                                <Typography variant="h6" gutterBottom>
                                    {this.props.content}
                                </Typography>
                                <SocialButtonsShare postId={this.props.id}/>
                            </div>
                        </div>
                    </div>
                </div>
                <Divider/>
            </div>

        )
    };
}

export default PostCard