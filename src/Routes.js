import React from 'react';
import ReactRouter from 'react-router';
import AboutPage from './pages/about/AboutPage'
import PostPage from './pages/post-page/PostPage'

var Router = ReactRouter.Router;
var Route = ReactRouter.Route;

var Routes = (
		<Router>
			<Route path="/" component={App}>
				<Route path="/about" component={AboutPage}/>
				<Route path="/posts/:id" component{PostPage}/>
			</Route>
		</Router>
	);

module.exports = Routes;