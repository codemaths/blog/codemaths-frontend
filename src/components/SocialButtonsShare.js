import React, {Component} from "react";
import '../containers/Styles/SocialButtonsShare.css'
import {
    FacebookShareButton,
    LinkedinShareButton,
    PinterestShareButton,
    RedditShareButton,
    TwitterShareButton
} from "react-share";


class SocialButtonsShare extends Component {
    render() {
        return (
            <div className="post-page-share-buttons">
                <link rel="stylesheet"
                      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
                <FacebookShareButton className="fa fa-facebook"
                                     url={`https://codemaths.co.uk/posts/${this.props.postId}`}/>

                <LinkedinShareButton className="fa fa-linkedin"
                                     url={`https://codemaths.co.uk/posts/${this.props.postId}`}/>

                <PinterestShareButton className="fa fa-pinterest"
                                      url={`https://codemaths.co.uk/posts/${this.props.postId}`}
                                      media={'http://www.indiaeducation.net/imagesvr_ce/8827/Software%20Engineering%201.jpg'}/>

                <TwitterShareButton className="fa fa-twitter"
                                    title="Here is a post by Codemaths"
                                    url={`https://codemaths.co.uk/posts/${this.props.postId}`}
                                    hashtags={["codemaths", "mathematics"]}/>

                <RedditShareButton className="fa fa-reddit"
                                   url={`https://codemaths.co.uk/posts/${this.props.postId}`}/>

            </div>
        )
    }
}

export default SocialButtonsShare;