import React from 'react';

export const MarkedInput = ({
                                value,
                                onChange,
                                id,
                                classNames
                            }) => (
    <div className={classNames.textContainer}>
    <textarea required
        id={id}
        onChange={onChange}
        value={value} />
    </div>
);