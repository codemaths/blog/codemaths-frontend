/*
  Import React
*/
import React, {Component} from "react";
import {MarkedInput} from "./MarkedInput";
import {MarkedPreview} from "./MarkedPreview";

/*
  MarkedArea Container Class
*/
export class CustomMarkedArea extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value
        };
    }

    static defaultProps = {
        id: 'mmc-marked-area',
        label: '',
        classNames: {
            root: 'marked-area',
            header: 'marked-area-header',
            activeButton: 'marked-area-button active',
            defaultButton: 'marked-area-button',
            helpLink: 'marked-area-help-link',
            textContainer: 'marked-area-text-container',
            liveDivider: 'marked-area-live-divider'
        }
    };

    componentWillReceiveProps(props) {
        this.setState({value: props.value});
    };

    handleTextChange = (e) => {
        if (e.target.value.length < this.props.characterLimit) {
            this.setState({value: e.target.value});
            this.props.onChange(e.target.value)

        }
    };

    render() {
        let {id, label, classNames, placeholder} = this.props;
        let {value} = this.state;
        return (
            <section className={classNames.root}>

                <header className={classNames.header}>
                    <label htmlFor={id}>{label}</label>
                </header>

                <MarkedInput
                    placeholder={placeholder}
                    classNames={classNames}
                    onChange={this.handleTextChange}
                    value={value}/>
                <p>Character limit {this.props.characterLimit}; Used {this.state.value.length}</p>

                {this.props.showPreview && <MarkedPreview classNames={classNames}
                               value={value}/>}

            </section>
        );
    }
}