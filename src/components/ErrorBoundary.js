import React, {Component} from 'react';

class ErrorBoundary extends Component {
	constructor(props){
		super(props);
		this.state = {
			hasError: false
		}
	}

	componentDidCatch(error, info){
		this.setState({hasError:true})
	}

	render(){
		return <h1>This is an error</h1>
	}
}

export default ErrorBoundary;