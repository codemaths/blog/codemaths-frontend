import React, {Component} from "react";

class SearchBox extends Component {
    render() {
        return (
            <div className='pa2'>
                <input
                    className='pa3 ba b--white bg-lightest-blue'
                    type='search'
                    placeholder='Search posts.'
                    onChange={this.props.searchChange}/>
            </div>
        )
    }

}

export default SearchBox;