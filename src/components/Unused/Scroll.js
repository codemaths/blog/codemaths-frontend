import React, {Component} from 'react'


class Scroll extends Component {
    render() {
        return (
            <div style={{overflowY:'scroll', border: 'none', height:'700px'}} >
                {this.props.children}s
            </div>
        )
    }
}

export default Scroll;