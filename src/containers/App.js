import ReactGA from 'react-ga';
import React, {Component} from 'react';
import LayoutContainer from './LayoutContainer'
import './Styles/App.css';

function initializeReactGA() {
    ReactGA.initialize('UA-159815113-1');
    ReactGA.pageview(window.location.pathname + window.location.search);
}

class App extends Component {

    render() {
        if (process.env.REACT_APP_ENV === "production") {
            initializeReactGA();
        }
        return (
            <div>
                <LayoutContainer store={this.props.store}/>
            </div>
        )
    };
}

export default App;
