import React, {Component} from 'react'
import PostCardList from '../pages/main/PostCardList';
import ErrorBoundary from '../components/ErrorBoundary'
import AboutPage from '../pages/about/AboutPage'
import NavTabs from '../components/NavTabs'
import Footer from '../components/Footer'
import PostPage from '../pages/post-page/PostPage';
import {BrowserRouter, Route, Switch} from 'react-router-dom';


class LayoutContainer extends Component {


    //when you create your own custom method always create it in this format name = (input) => {}
    // onSearchChange = (event) => {
    //     this.setState({searchField: event.target.value})
    // };

    //one of the LIFE CYCLE methods inside React.Component


    render() {
        return (<div className="tc">
                <BrowserRouter>
                    <div>
                        <NavTabs/>
                        <Switch>
                            <Route path="/" component={PostCardList} exact/>
                            <Route path="/about" component={AboutPage} exact/>
                            <Route path="/posts/:id" component={PostPage}/>
                            <Route component={ErrorBoundary}/>
                        </Switch>
                    </div>
                </BrowserRouter>
                <Footer/>
            </div>
        )

    }

}

export default LayoutContainer
